	const shell = require('shelljs');

	$('#MintUpdate').click(function(){
		shell.exec('mintupdate',()=>{})
	});

	$('#DuzeruKernel').click(function(){
		shell.exec('gksu /opt/dki/dki',()=>{})
	});

	$('#xfpanelSwitch').click(function(){
		shell.exec('xfpanel-switch',()=>{})
	});

	$('#duzeruControlCenter').click(function(){
		shell.exec('/opt/duzeru-ccenter/duzeru-ccenter',()=>{})
	});

	$('#Desktop').click(function(){
		shell.exec('xfdesktop-settings',()=>{})
	});

	$('#stacer').click(function(){
		shell.exec('stacer',()=>{})
	});

	$('#MintInstall').click(function(){
		shell.exec('gksu /usr/bin/mintinstall',()=>{})
	});

	shell.exec('find /home/$USER/.config/autostart/ -name welcomedz.desktop', (a, b, c)=>{
		if(b == '')
		$('#offinitialize').prop('checked', false)
		else
		$('#offinitialize').prop('checked', true)
	});

	$('#offinitialize').click(function(){

		if($('#offinitialize').prop('checked') == true){
			shell.exec('cp /opt/welcomedz/welcomedz.desktop //home/$USER/.config/autostart/welcomedz.desktop',()=>{})
		} else {
			shell.exec('rm -rf /home/$USER/.config/autostart/welcomedz.desktop',()=>{})
		}

	});

	$(document).ready(function(){
		$('.tabs').tabs();
	});
